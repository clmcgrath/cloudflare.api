﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Purge cache related request
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - zones: {ZoneCheckResult}
    ///             - obj: {Dictionary{String, int}}
    ///             
    /// </summary>
    public class ZoneCheckResponse : ResponseBase
    {
        public ZoneCheckResult response { get; set; }
    }

    public class ZoneCheckResult
    {
        public Dictionary<String, int> zones { get; set; }
    }
}
