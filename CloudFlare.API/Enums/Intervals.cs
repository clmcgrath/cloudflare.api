﻿namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Intervals for Statistics
    /// </summary>
    public enum Intervals
    {
        Days30 = 20,
        Days7 = 30,
        Yesterday = 40,
        Hours24 = 100,
        Hours12 = 110,
        Hours6 = 120
    }
}
