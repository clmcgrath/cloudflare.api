﻿namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Status for the developer mode
    /// </summary>
    /// 9/7/2013 by Sergi
    public enum DeveloperMode
    {
        Off = 0,
        On = 1
    }
}
